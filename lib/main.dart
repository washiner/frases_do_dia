import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.purple,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Frases do dia'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _numeroAleatorio = 0;
  List _frases = [
    "Aquilo que se faz por amor está sempre além do bem e do mal.",
    "A vida não é um problema a ser resolvido, mas uma realidade a ser experimentada.",
    "A vida só pode ser compreendida, olhando-se para trás; mas só pode ser vivida, olhando-se para frente.",
    "E aqueles que foram vistos dançando foram julgados insanos por aqueles que não podiam escutar a música.",
  ];

  void _incrementCounter() {
    setState(() {
      _numeroAleatorio = Random().nextInt(4);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 50),
            child: Image(
                image: AssetImage("lib/assets/yang.png"),
              //height: 200,
              width: 200,
              //fit: BoxFit.scaleDown,
              //fit: BoxFit.fill,
              //color: Colors.red,
              //colorBlendMode: BlendMode.darken,
              semanticLabel: "Yang",
              //loadingBuilder: (context, child, progress){
                 // return progress == null ? child : LinearProgressIndicator(backgroundColor: Colors.red);
              //},
            ),
          ),
          SizedBox(height: 30),
          Text(
            'Precione para gerar uma frase:',
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                letterSpacing: 2,
                wordSpacing: 2,
                //decorationStyle: TextDecorationStyle.dashed,
                //decorationColor: Colors.yellow,
                //decoration: TextDecoration.underline,
                //decorationThickness: 3
              ),
          ),
          SizedBox(height: 50),
          Container(
            width: double.infinity,
            height: 100,
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Text(
                //'$_numeroAleatorio',
                _frases[_numeroAleatorio], textAlign: TextAlign.center,
                style: TextStyle(fontSize: 22),
              ),
            ),
          ),
          SizedBox(height: 20),
          Container(
            width: double.infinity,
            height: 50,
            child: RaisedButton(
              color: Theme.of(context).primaryColor,
              child: Text("Click aqui ... para gerar uma nova frase",
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                    color: Colors.white
                ),
              ),
                onPressed: (){
                _incrementCounter();
                }
                ),
          ),
        ],
      ),
      // floatingActionButton: FloatingActionButton(
      //   onPressed: _incrementCounter,
      //   tooltip: 'Increment',
      //   child: Icon(Icons.add),
      // ),
    );
  }
}
